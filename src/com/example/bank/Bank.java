package com.example.bank;

public class Bank {
    public static void main(String[] args) {
        try {
            Account account = new Account("GB821234", "Swapnil Mistry", 0);
            double balance = account.getAccountBalance();
            System.out.println(balance); // Print current balance
            account.deposit(10000);
            double balanceAfterDeposit = account.getAccountBalance();
            System.out.println(balanceAfterDeposit); // Print account balance after deposit
            account.withdraw(20000);
        }
        catch (Exception e) {
            System.out.println(e.getMessage()); // Print custom error message
        }
    }
}
