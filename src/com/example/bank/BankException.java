package com.example.bank;

public class BankException extends Exception {
    public BankException(String errorMessage) {
        super(errorMessage);
    }
}
