package com.example.bank;

public class Account {
    private String accountNumber;
    private String ownerName;
    private double accountBalance;

    public Account(String accountNumber, String ownerName) throws IllegalArgumentException {
        if (!verifyAccountNumber(accountNumber)) {
            throw new IllegalArgumentException("Invalid account number.");
        }
        this.accountNumber = accountNumber;
        this.ownerName = ownerName;
        this.accountBalance = 0;
    }

    public Account(String accountNumber, String ownerName, double initialBalance) throws IllegalArgumentException {
        if (!verifyAccountNumber(accountNumber)) {
            throw new IllegalArgumentException("Invalid account number.");
        }
        this.accountNumber = accountNumber;
        this.ownerName = ownerName;
        this.accountBalance = initialBalance;
    }

    public void deposit(double amountToDeposit) {
        this.accountBalance = this.accountBalance + amountToDeposit;
    }

    public void withdraw(double amountToWithdraw) throws BankException {
        if (amountToWithdraw > this.accountBalance) {
            throw new BankException("Insufficient funds to withdraw " +amountToWithdraw+ ", current balance " +this.accountBalance);
        }
    }

    public double getAccountBalance() {
        return this.accountBalance;
    }

    private boolean verifyAccountNumber(String accountNumber) {
        if (accountNumber.length() != 8) {
            return false;
        }
        char ch[] = accountNumber.toCharArray();
        String firstFourChars = this.getInterpretedChars(ch);
        String otherChars = this.getOtherChars(ch);
        String interpretedAccountNumber = otherChars + firstFourChars;
        double number = Double.parseDouble(interpretedAccountNumber);
        return isValid(number);
    }

    private boolean isValid(double number) {
        if (number % 11 == 1) {
            return true;
        }
        return  false;
    }
    private String getInterpretedChars(char[] items) {
        String chars = "";
        for (int i = 0; i < items.length / 2; i++) {
            if (i < 2) {
                chars = chars + getIndexForChar(items[i]);
            } else {
                chars = chars + items[i];
            }
        }
        return chars;
    }
    private int getIndexForChar(char letter) {
        int defaultCharIndex = 'A';
        int charIndex = letter;
        return Math.abs(charIndex - defaultCharIndex) + 10;
    }
    private String getOtherChars(char[] ch) {
        String chars = "";
        for (int i = ch.length / 2; i < ch.length; i++) {
            chars = chars + ch[i];
        }
        return chars;
    }
}
